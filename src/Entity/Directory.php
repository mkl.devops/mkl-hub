<?php

namespace App\Entity;

use App\Repository\DirectoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fardus\Traits\Symfony\Entity\IdEntityTrait;
use Fardus\Traits\Symfony\Entity\NameEntityTrait;
use Fardus\Traits\Symfony\Entity\TimestampableEntityTrait;

/**
 * @ORM\Entity(repositoryClass=DirectoryRepository::class)
 */
class Directory
{
    use IdEntityTrait;
    use NameEntityTrait;
    use TimestampableEntityTrait;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $path = '/';

    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="directories")
     */
    private ?Directory $directory = null;

    /**
     * @ORM\OneToMany(targetEntity=Directory::class, mappedBy="directory")
     */
    private Collection $directories;

    /**
     * @ORM\OneToMany(targetEntity=File::class, mappedBy="directory")
     */
    private Collection $files;

    public function __construct()
    {
        $this->directories = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getDirectory(): ?self
    {
        return $this->directory;
    }

    public function setDirectory(?self $directory): self
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getDirectories(): Collection
    {
        return $this->directories;
    }

    public function addDirectory(self $directory): self
    {
        if (!$this->directories->contains($directory)) {
            $this->directories[] = $directory;
            $directory->setDirectory($this);
        }

        return $this;
    }

    public function removeDirectory(self $directory): self
    {
        // set the owning side to null (unless already changed)
        if ($this->directories->removeElement($directory) && $directory->getDirectory() === $this) {
            $directory->setDirectory(null);
        }

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(File $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setDirectory($this);
        }

        return $this;
    }

    public function removeFile(File $file): self
    {
        // set the owning side to null (unless already changed)
        if ($this->files->removeElement($file) && $file->getDirectory() === $this) {
            $file->setDirectory(null);
        }

        return $this;
    }
}
