<?php

namespace App\Controller;

use App\Entity\File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/file')]
class FileController extends AbstractController
{
    #[Route('/generate/link/{name}', name: 'file_generate_link')]
    public function generateLink(string $name = null): Response
    {
        $file = (new File())->setName($name ?? md5(time()));
        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->persist($file);
        $doctrine->flush();

        return $this->redirectToRoute('file_show', ['id' => $file->getId()]);
    }

    #[Route('/show/{id}', name: 'file_show', methods: ['GET'])]
    public function show(File $file): Response
    {
        return $this->render('file/show.html.twig', [
            'file' => $file
        ]);
    }

    #[Route('/download/{id}', name: 'file_download', methods: ['GET'])]
    public function download(File $file): Response
    {
        return $this->file($file->getPath());
    }
}
