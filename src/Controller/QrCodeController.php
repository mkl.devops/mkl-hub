<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/qr-code')]
class QrCodeController extends AbstractController
{
    #[Route('/download/{path}', name: 'qr_code_download', requirements: ['path' => '.*'], methods: ['GET'])]
    public function download(string $path): Response
    {
        if(!file_exists($path)) {
            return $this->json(['Error' => 'No such file '.$path], Response::HTTP_NOT_FOUND);
        }
        return $this->file($path);
    }

    #[Route('/generate', name: 'qr_code_generate', methods: ['GET'])]
    public function generate(Request $request): Response
    {
        $path = $request->get('path');
        return $this->render('qr_code/generate.html.twig', [
            'path' => $path,
            'path_encoded' => urlencode($path),
        ]);
    }
}
