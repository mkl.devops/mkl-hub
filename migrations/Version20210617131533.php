<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210617131533 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL, is_verified BOOLEAN NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('DROP INDEX IDX_467844DA2C94069F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__directory AS SELECT id, directory_id, path, name, created_at, updated_at FROM directory');
        $this->addSql('DROP TABLE directory');
        $this->addSql('CREATE TABLE directory (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, directory_id INTEGER DEFAULT NULL, path VARCHAR(255) NOT NULL COLLATE BINARY, name VARCHAR(255) DEFAULT NULL COLLATE BINARY, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, CONSTRAINT FK_467844DA2C94069F FOREIGN KEY (directory_id) REFERENCES directory (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO directory (id, directory_id, path, name, created_at, updated_at) SELECT id, directory_id, path, name, created_at, updated_at FROM __temp__directory');
        $this->addSql('DROP TABLE __temp__directory');
        $this->addSql('CREATE INDEX IDX_467844DA2C94069F ON directory (directory_id)');
        $this->addSql('DROP INDEX IDX_8C9F36102C94069F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__file AS SELECT id, directory_id, size, mimetype, name, created_at, updated_at FROM file');
        $this->addSql('DROP TABLE file');
        $this->addSql('CREATE TABLE file (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, directory_id INTEGER DEFAULT NULL, size INTEGER NOT NULL, mimetype VARCHAR(30) NOT NULL COLLATE BINARY, name VARCHAR(255) DEFAULT NULL COLLATE BINARY, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, CONSTRAINT FK_8C9F36102C94069F FOREIGN KEY (directory_id) REFERENCES directory (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO file (id, directory_id, size, mimetype, name, created_at, updated_at) SELECT id, directory_id, size, mimetype, name, created_at, updated_at FROM __temp__file');
        $this->addSql('DROP TABLE __temp__file');
        $this->addSql('CREATE INDEX IDX_8C9F36102C94069F ON file (directory_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP INDEX IDX_467844DA2C94069F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__directory AS SELECT id, directory_id, path, name, created_at, updated_at FROM directory');
        $this->addSql('DROP TABLE directory');
        $this->addSql('CREATE TABLE directory (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, directory_id INTEGER DEFAULT NULL, path VARCHAR(255) NOT NULL, name VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO directory (id, directory_id, path, name, created_at, updated_at) SELECT id, directory_id, path, name, created_at, updated_at FROM __temp__directory');
        $this->addSql('DROP TABLE __temp__directory');
        $this->addSql('CREATE INDEX IDX_467844DA2C94069F ON directory (directory_id)');
        $this->addSql('DROP INDEX IDX_8C9F36102C94069F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__file AS SELECT id, directory_id, size, mimetype, name, created_at, updated_at FROM file');
        $this->addSql('DROP TABLE file');
        $this->addSql('CREATE TABLE file (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, directory_id INTEGER DEFAULT NULL, size INTEGER NOT NULL, mimetype VARCHAR(30) NOT NULL, name VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL)');
        $this->addSql('INSERT INTO file (id, directory_id, size, mimetype, name, created_at, updated_at) SELECT id, directory_id, size, mimetype, name, created_at, updated_at FROM __temp__file');
        $this->addSql('DROP TABLE __temp__file');
        $this->addSql('CREATE INDEX IDX_8C9F36102C94069F ON file (directory_id)');
    }
}
